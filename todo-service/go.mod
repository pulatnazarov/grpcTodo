module todo-service

go 1.19

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.2
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.6
	google.golang.org/grpc v1.45.0
	google.golang.org/protobuf v1.27.1
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220317061510-51cd9980dadf // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20220314164441-57ef72a4c106 // indirect
)
