package grpc_server

import (
	"context"
	"log"
	"todo-service/service"
	"todo-service/todo"
	"todo-service/todopb"
)

func NewGRPCServer(svc service.Service) *GRPCServer {
	return &GRPCServer{
		svc: svc,
	}
}

type GRPCServer struct {
	todopb.UnimplementedTodoServiceServer
	svc service.Service
}

func (g *GRPCServer) GetTodoByUserId(ctx context.Context, userId *todopb.UserIdRequest) (*todopb.Todos, error) {
	ts, er := g.svc.GetTodosByUser(ctx, userId.GetId())
	if er != nil {
		return nil, er
	}

	return toProtoTodos(ts), nil
}

func toProtoTodo(t todo.Todo) *todopb.Todo {
	return &todopb.Todo{
		Id:     t.Id,
		Body:   t.Body,
		Done:   t.Done,
		UserId: t.UserId,
	}
}

func toProtoTodos(ts []todo.Todo) *todopb.Todos {
	protoTodos := make([]*todopb.Todo, 0, len(ts))
	for _, t := range ts {
		protoTodos = append(protoTodos, toProtoTodo(t))
	}
	return &todopb.Todos{
		List: protoTodos,
	}
}

func (g *GRPCServer) GetTodo(ctx context.Context, tId *todopb.TodoIdRequest) (*todopb.Todo, error) {
	t, er := g.svc.GetTodo(ctx, tId.GetId())
	if er != nil {
		return nil, er
	}

	return &todopb.Todo{
		Id:     t.Id,
		Body:   t.Body,
		Done:   t.Done,
		UserId: t.UserId,
	}, nil
}

func (g *GRPCServer) CreateTodo(ctx context.Context, t *todopb.NewTodo) (*todopb.TodoIdResponse, error) {
	tId, er := g.svc.CreateTodo(ctx, t.Body, t.UserId)
	if er != nil {
		return nil, er
	}

	return &todopb.TodoIdResponse{
		Id: tId,
	}, nil
}

func (g *GRPCServer) UpdateTodo(ctx context.Context, t *todopb.Todo) (*todopb.TodoIdResponse, error) {
	tId, er := g.svc.UpdateTodo(ctx, todo.Todo{
		Id:     t.Id,
		Body:   t.Body,
		Done:   t.Done,
		UserId: t.UserId,
	})

	if er != nil {
		log.Println("5: ", er)
		return nil, er
	}

	return &todopb.TodoIdResponse{
		Id: tId,
	}, nil
}
