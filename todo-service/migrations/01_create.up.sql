create table todos (
    id uuid not null primary key,
    body text not null,
    done boolean,
    user_id varchar(40) not null
);