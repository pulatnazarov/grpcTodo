package services

import (
	"api-gateway/structs"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type UserServiceClient interface {
	GetUserById(ctx context.Context, userId string) (structs.User, error)
	RegisterUser(ctx context.Context, name string) (string, error)
	UpdateUser(ctx context.Context, userId, userName string) (string, error)
}

type userServiceAdapter struct {
	url    string
	client *http.Client
}

func (a userServiceAdapter) GetUserById(ctx context.Context, userId string) (u structs.User, er error) {
	request, er := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("%s/user/%s", a.url, userId), nil)
	if er != nil {
		fmt.Println("3: ", er)
		return structs.User{}, er
	}

	resp, er := a.client.Do(request)
	if er != nil {
		fmt.Println("4: ", er)
		return structs.User{}, er
	}

	if er = json.NewDecoder(resp.Body).Decode(&u); er != nil {
		fmt.Println("5: ", er)
		return structs.User{}, er
	}
	return u, nil
}

func (a userServiceAdapter) RegisterUser(ctx context.Context, name string) (string, error) {
	request, er := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/user/%s", a.url, name), nil)
	if er != nil {
		return "", er
	}

	resp, er := a.client.Do(request)
	if er != nil {
		return "", er
	}

	userId := ""

	if er = json.NewDecoder(resp.Body).Decode(&userId); er != nil {
		return "", er
	}
	return userId, nil
}

func (a userServiceAdapter) UpdateUser(ctx context.Context, userId, userName string) (string, error) {
	request, er := http.NewRequestWithContext(ctx, http.MethodPut, fmt.Sprintf("%s/user/update/%s/%s", a.url, userId, userName), nil)
	if er != nil {
		log.Println("2: ", er)
		return "", er
	}

	resp, er := a.client.Do(request)
	if er != nil {
		log.Println("3: ", er)
		return "", er
	}
	name := ""
	if er := json.NewDecoder(resp.Body).Decode(&name); er != nil {
		log.Println("4: ", er)
		return "", er
	}

	return name, nil
}
