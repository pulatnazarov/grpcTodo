package services

import (
	"api-gateway/structs"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type TodoServiceClient interface {
	GetTodosByUserId(ctx context.Context, userId string) ([]structs.Todo, error)
	GetTodo(ctx context.Context, todoId string) (structs.Todo, error)
	RegisterTodo(ctx context.Context, rBody io.Reader) (string, error)
	UpdateTodo(ctx context.Context, rBody io.Reader) (string, error)
}

type todoServiceAdapter struct {
	url    string
	client *http.Client
}

func (a todoServiceAdapter) UpdateTodo(ctx context.Context, rBody io.Reader) (string, error) {
	request, er := http.NewRequestWithContext(ctx, http.MethodPut, fmt.Sprintf("%s/todo/update", a.url), rBody)
	if er != nil {
		return "", er
	}

	resp, er := a.client.Do(request)
	if er != nil {
		return "", er
	}

	todoId := ""

	if er = json.NewDecoder(resp.Body).Decode(&todoId); er != nil {
		return "", er
	}
	return todoId, nil

}

func (a todoServiceAdapter) GetTodosByUserId(ctx context.Context, userId string) ([]structs.Todo, error) {
	request, er := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("%s/todo/user/%s", a.url, userId), nil)
	if er != nil {
		fmt.Println("6: ", er)
		return nil, er
	}

	resp, er := a.client.Do(request)
	if er != nil {
		fmt.Println("7: ", er)
		return nil, er
	}

	todos := make([]structs.Todo, 0, 2)
	if er = json.NewDecoder(resp.Body).Decode(&todos); er != nil {
		fmt.Println("8: ", er)
		return nil, er
	}
	return todos, nil
}

func (a todoServiceAdapter) GetTodo(ctx context.Context, todoId string) (structs.Todo, error) {
	request, er := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("%s/todo/%s", a.url, todoId), nil)
	if er != nil {
		return structs.Todo{}, er
	}

	resp, er := a.client.Do(request)
	if er != nil {
		return structs.Todo{}, er
	}
	todo := structs.Todo{}
	if er = json.NewDecoder(resp.Body).Decode(&todo); er != nil {
		return structs.Todo{}, er
	}
	return todo, nil
}

func (a todoServiceAdapter) RegisterTodo(ctx context.Context, rBody io.Reader) (string, error) {
	request, er := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/todo", a.url), rBody)
	if er != nil {
		return "", er
	}

	resp, er := a.client.Do(request)
	if er != nil {
		return "", nil
	}

	todoId := ""
	if er = json.NewDecoder(resp.Body).Decode(&todoId); er != nil {
		return "", er
	}
	return todoId, nil
}
