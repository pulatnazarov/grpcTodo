package services

import (
	"api-gateway/services_grpc"
)

func NewServices(userServiceGRPCURL, todoServiceGRPCURL string) Services {
	return Services{
		UserService: services_grpc.NewUserServiceGRPCClient(userServiceGRPCURL),
		TodoService: services_grpc.NewTodoServiceGRPCClient(todoServiceGRPCURL),
	}
}

type Services struct {
	UserService UserServiceClient
	TodoService TodoServiceClient
}
