package handler

import (
	"api-gateway/services"
	"api-gateway/structs"
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"log"
	"net/http"
)

type Handler struct {
	services services.Services
}

func New(services services.Services) Handler {
	return Handler{
		services: services,
	}
}

func (h Handler) GetUserTodos(w http.ResponseWriter, r *http.Request) {
	userId := chi.URLParam(r, "user-id")

	// step 1
	user, er := h.services.UserService.GetUserById(context.Background(), userId)
	if er != nil {
		fmt.Println("1: ", er)
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}
	/*
		response, er := http.Get(fmt.Sprintf("http://localhost:9009/user/%s", userId))
		if er != nil {
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, er)
		}
		if er = json.NewDecoder(response.Body).Decode(&u); er != nil {
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, er)
		}
	*/

	//-----------------------------------

	// step 2
	todos, er := h.services.TodoService.GetTodosByUserId(context.Background(), userId)
	if er != nil {
		fmt.Println("2: ", er)
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}
	/*
		response, er = http.Get(fmt.Sprintf("http://localhost:9000/todo/user/%s", userId))
		if er != nil {
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, er)
		}
		if er = json.NewDecoder(response.Body).Decode(&t); er != nil {
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, er)
		}
	*/

	// step 3
	render.JSON(w, r, structs.Result{
		User:  user,
		Todos: todos,
	})
	/*
		result := Result{
			User: u,
			Todo: t,
		}
		render.JSON(w, r, result)
	*/
}

func (h Handler) GetTodo(w http.ResponseWriter, r *http.Request) {
	todoId := chi.URLParam(r, "todo-id")

	todo, er := h.services.TodoService.GetTodo(context.Background(), todoId)
	if er != nil {
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}

	render.JSON(w, r, todo)
}

func (h Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	userId := chi.URLParam(r, "user-id")

	user, er := h.services.UserService.GetUserById(context.Background(), userId)
	if er != nil {
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}

	render.JSON(w, r, user)
}

func (h Handler) RegisterUser(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	userId, er := h.services.UserService.RegisterUser(context.Background(), name)
	if er != nil {
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}

	render.JSON(w, r, userId)
}

func (h Handler) RegisterTodo(w http.ResponseWriter, r *http.Request) {
	todoId, er := h.services.TodoService.RegisterTodo(context.Background(), r.Body)
	if er != nil {
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}

	render.JSON(w, r, todoId)
}

func (h Handler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	userId := chi.URLParam(r, "user-id")
	userName := chi.URLParam(r, "name")
	name, er := h.services.UserService.UpdateUser(context.Background(), userId, userName)
	if er != nil {
		log.Println("1: ", er)
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}

	render.JSON(w, r, name)
}

func (h Handler) UpdateTodo(w http.ResponseWriter, r *http.Request) {
	todoId, er := h.services.TodoService.UpdateTodo(context.Background(), r.Body)
	if er != nil {
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, er)
		return
	}

	render.JSON(w, r, todoId)
}
