package psql

import (
	"context"
	"database/sql"
	"user-service/user"
)

type Postgres struct {
	db *sql.DB
}

func New(db *sql.DB) Postgres {
	return Postgres{
		db: db,
	}
}

type PsqlRepo interface {
	InsertUser(ctx context.Context, user user.User) error
	ListUsers(ctx context.Context) ([]user.User, error)
	GetUser(ctx context.Context, userId string) (user.User, error)
	UpdateUser(ctx context.Context, uId, uName string) error
}

func (p Postgres) UpdateUser(ctx context.Context, uId, uName string) error {
	if _, er := p.db.ExecContext(ctx, `update users set name = $1 where id = $2`, uName, uId); er != nil {
		return er
	}
	return nil
}

func (p Postgres) InsertUser(ctx context.Context, user user.User) error {
	if _, er := p.db.ExecContext(ctx, `insert into users values ($1, $2)`, user.Id, user.Name); er != nil {
		return er
	}
	return nil
}

func (p Postgres) ListUsers(ctx context.Context) ([]user.User, error) {
	var users []user.User

	rows, er := p.db.QueryContext(ctx, `select * from users`)
	if er != nil {
		return nil, er
	}

	for rows.Next() {
		u := user.User{}
		if er = rows.Scan(&u.Id, &u.Name); er != nil {
			return nil, er
		}
		users = append(users, u)
	}
	return users, nil
}

func (p Postgres) GetUser(ctx context.Context, userId string) (user.User, error) {
	var u user.User
	if er := p.db.QueryRowContext(ctx, `select * from users where id = $1`, userId).Scan(&u.Id, &u.Name); er != nil {
		return user.User{}, er
	}
	return u, nil
}
