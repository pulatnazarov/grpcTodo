package main

import (
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"log"
	"net"
	"user-service/config"
	"user-service/grpc_server"
	"user-service/postgres"
	"user-service/psql"
	"user-service/service"
	"user-service/userpb"
)

func main() {
	cfg := config.Load()
	db, er := postgres.Connect(cfg)
	if er != nil {
		log.Fatalf("error with connectDb: %v", er)
	}
	repo := psql.New(db)
	svc := service.New(repo)
	//api.NewRouter(cfg, svc)

	lis, er := net.Listen("tcp", "localhost:9009")
	if er != nil {
		log.Fatalf("1: %v", er)
	}

	s := grpc.NewServer()
	userpb.RegisterUserServiceServer(s, grpc_server.NewGRPCServer(svc))

	if er = s.Serve(lis); er != nil {
		log.Fatalf("2: %v", er)
	}
}
