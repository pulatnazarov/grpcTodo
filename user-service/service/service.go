package service

import (
	"context"
	"user-service/psql"
	"user-service/user"
)

type ServiceRepo interface {
	CreateUser(ctx context.Context, name string) (string, error)
	ListUsers(ctx context.Context) ([]user.User, error)
	GetUser(ctx context.Context, userId string) (user.User, error)
	UpdateUser(ctx context.Context, uName, uId string) error
	//DeleteUser(ctx context.Context, id string) error
}

type Service struct {
	psql psql.PsqlRepo
}

func New(psql psql.PsqlRepo) Service {
	return Service{
		psql: psql,
	}
}

func (s Service) UpdateUser(ctx context.Context, uId, uName string) error {
	er := s.psql.UpdateUser(ctx, uId, uName)
	return er
}

func (s Service) CreateUser(ctx context.Context, name string) (string, error) {
	user := user.New(name)
	if er := s.psql.InsertUser(ctx, user); er != nil {
		return "", er
	}
	return user.Id, nil
}

func (s Service) ListUsers(ctx context.Context) ([]user.User, error) {
	users, er := s.psql.ListUsers(ctx)
	if er != nil {
		return nil, er
	}
	return users, nil
}

func (s Service) GetUser(ctx context.Context, userId string) (user.User, error) {
	u, er := s.psql.GetUser(ctx, userId)
	if er != nil {
		return user.User{}, er
	}
	return u, nil
}
